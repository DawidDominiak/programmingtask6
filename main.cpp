#include <iostream> //Wczytanie zależności
#include <fstream>  
#include <string>  
#include <iomanip>
#include <cstdlib>
#include <list>
#include <cfloat>

using namespace std; //Używamy przestrzeni nazw std. Nie występują w kodzie żadne kolizje nazw.
/*
* Główna funkcja inicjalizująca program
*/
int main() {
	const int n = 3, precision = 4; //definicja n i precision
	string filename; //deklaracja filename
	double main_diagonal_min = DBL_MAX; //definicja wartości minimum głównej przekątnej
	double local_highest_element = -DBL_MAX; //definicja wartości lokalnego maksimum w wierszu
	list<int> indexes; //definicja listy indeksów
	double A[n][n]; //definicja tablicy n x n z danymi

	//Wczytywanie nazwy pliku
	cout << "Podaj nazwe pliku: ";
	cin >> filename;
	cout << endl;

	//Otwieranie pliku w trybie do czytanie
	ifstream file(filename.c_str(), ios::in);

	//Przechwycenie błędu otwarcia
	if(!file.is_open()) {
		cerr << "Pojawil sie problem z otwarciem pliku";
		exit(1);
	}

	//Wypełnienie tablicy A danymi z pliku
	for(int i = 0; i < n; i++) {
		for(int j = 0; j < n; j++) {
			file >> A[i][j];
		}
	}

	//Wyświetlenie tablicy
	for(int i = 0; i < n; i++) {
		for(int j = 0; j < n; j++) {
			cout << fixed << setprecision(precision) << setw(8) << A[i][j];
		}
		cout << endl;
	}

	//Znalezienie minimum głównej przekątnej
	for(int i = 0; i < n; i++) {
		if(A[i][i] < main_diagonal_min) {
			main_diagonal_min = A[i][i];
		}
	}
	
	//Iteracja po wierszach
	for(int i = 0; i < n; i++) {
		// Znalezienie największych lokalnie elementów
		for(int j = 0; j < i; j++) {
			if(A[i][j] >= local_highest_element) {
				local_highest_element = A[i][j];
				indexes.push_back(j);
			}
		}

		//Ustawienie wartości tych elementów na minimum głównej przekątnej
		for( list<int>::iterator iter=indexes.begin(); iter != indexes.end(); ) {
			A[i][*iter] = main_diagonal_min;
			iter++;
		}
		
		//Wyczyszczenie listy indeksów i lokalnej najwyższej wartości
		indexes.clear();
		local_highest_element = -DBL_MAX;
	}

	cout << endl << endl;

	//Wyświetlenie tablicy ponownie
	for(int i = 0; i < n; i++) {
		for(int j = 0; j < n; j++) {
			cout << fixed << setprecision(precision) << setw(8) << A[i][j];
		}
		cout << endl;
	}

	return 0;
}